
class Item
  attr_accessor :content

  def idents
    ids = []
    if self.content['prompts']
      id = self.content['item_id']
      self.content['prompts'].each_with_index do |p, i|
        ids << id + '-' + (i+1).to_s
      end
    else
      ids << self.content['item_id']
    end
    return ids
  end

  # returns an array of one or more items
  def pretty_item_type_names
    item_type = self.content['item_type']
    item_pretty_name = {
      'item_audio_picture' => '聽',
      'item_cloze' => 'CLOZE',
      'item_dictal' => '聽寫',
      'item_fill_in_the_blanks' => '填空',
      'item_fill_in_the_blanks_mcq' => '單選題/填空',
      'item_listening_mcq' => '聽／單選題'
    }[item_type]
    item_length = self.idents.count
    item_names = []
    item_length.times do |i|
      if(item_length == 1)
        item_names << (item_pretty_name)
      else
        item_names << (item_pretty_name + ' ' + (i+1).to_s)
      end
    end
    item_names
  end

end
