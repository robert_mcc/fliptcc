class Chapter
  attr_reader :title
  attr_reader :copyright
  attr_reader :items
  attr_reader :item_idents
  attr_reader :id
  attr_reader :book_ident

  def initialize (book_name, chapter_name, copyright, id)
    yaml_items = YAML.load(File.read("db/seeds/#{book_name}/#{chapter_name}.yaml"))
    @title = book_name.gsub('-','_').humanize + ' '+ chapter_name.gsub('-','_').humanize
    @copyright = copyright
    @items = []
    @item_idents = []
    @id = id
    @book_ident = book_name
    yaml_items.each do |item|
      i = Item.new
      i.content = item
      @items << i
      # some "items" have more than one item_ident
      # This is because some items are really collections of items, like CLOZE with word bank
      @item_idents.concat(i.idents)
      # Check if there are any duplicates of item_idents
      # They should be unique, raise error otherwise.
      set_of_item_idents = Set.new @item_idents
      if(set_of_item_idents.size != @item_idents.length) 
        raise "Oops, there may be duplicates of your item_idents. Please double check your YAML!"
      end
      # @item_idents << i.idents
    end
  end

  def self.all
    chapter_list = YAML.load(File.read('db/seeds/chapters.yaml'))
    chapter_list.map {|yaml| Chapter.new yaml['book'], yaml['chapter'], yaml['copyright'], yaml['id']}
  end

  def self.all_as_hashes
    chapter_list = YAML.load(File.read('db/seeds/chapters.yaml'))
    chapter_list
  end

  def self.fetch id
    id = id.to_i # handle case of string from param
    yaml = self.all_as_hashes.select{|ch| ch['id'] == id}[0]
    self.new yaml['book'], yaml['chapter'], yaml['copyright'], yaml['id']
  end

  def lessons_count teacher
    Lesson.where(:teacher => teacher, :chapter_id => self.id).count
  end
end
