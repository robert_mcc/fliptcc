"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cloudant = require('cloudant');

var Database = function () {
  function Database() {
    _classCallCheck(this, Database);
  }

  _createClass(Database, [{
    key: 'connect',
    value: function connect(me, pass) {
      this.cld = Cloudant({ account: me, password: pass });
    }
  }, {
    key: 'createDb',
    value: function createDb(dbName) {
      var _this = this;

      this.name = dbName;
      return new Promise(function (resolve, reject) {
        _this.cld.db.create(dbName, function (err, body) {
          if (err) return reject(err);else return resolve(body);
        });
      });
    }
  }, {
    key: 'genKey',
    value: function genKey() {
      var _this2 = this;

      var promise = new Promise(function (resolve, reject) {
        _this2.cld.generate_api_key(function (err, api) {
          if (err) return err;else return resolve(api);
        });
      });
      return promise;
    }
  }, {
    key: 'authDbForKey',
    value: function authDbForKey(apiCred, dbName) {
      var security = {};
      dbName = dbName || this.dbName;
      security[apiCred.key] = ['_reader', '_writer', '_replicator'];
      // set security
      var my_database = this.cld.db.use(dbName);
      return new Promise(function (resolve, reject) {
        var getSecurityPromise = new Promise(function (resolve, reject) {
          my_database.get_security(function (err, result) {
            if (err) reject(err);else resolve(result.cloudant || {});
          });
        });
        getSecurityPromise.then(function (security) {
          my_database.set_security(security, function (err, result) {
            if (err) reject(err);else resolve(result);
          });
        });
      });
    }
  }, {
    key: 'genKeyForDb',
    value: function genKeyForDb(dbName) {
      var _this3 = this;

      var self = this;
      var apiCred = void 0;
      return this.genKey().then(function (api) {
        apiCred = api;
        return _this3.authDbForKey(apiCred, dbName);
      }).then(function () {
        return apiCred;
      });
    }
  }, {
    key: 'genKeyForMultiDBs',
    value: function genKeyForMultiDBs(DBs) {
      var _this4 = this;

      var self = this;
      var apiCred = void 0;
      return this.genKey().then(function (api) {
        apiCred = api;
        var promises = DBs.map(function (dbName) {
          return _this4.authDbForKey(apiCred, dbName);
        });
        return Promise.all(promises).then(function () {
          return apiCred;
        });
      });
    }
  }]);

  return Database;
}();

function createDbAndGetCreds(configuration, user) {

  var me = configuration.CLOUDANT_USER;
  var mypassword = configuration.CLOUDANT_SECRET;
  // const auth0Id = req.user.sub;
  // const dbName = auth0Id.replace("|", "_")
  var userEmail = user.email;
  var dbName = userEmail.replace("@", "()").replace(/\./g, '$').toLowerCase();
  console.log('creating: ' + dbName);

  var db = new Database();
  db.connect(me, mypassword);
  var dbNameZ = [dbName, dbName + '-images'];

  return db.createDb(dbName).then(function () {
    return db.createDb(dbNameZ[1]);
  }).then(function () {
    return db.genKeyForMultiDBs(dbNameZ);
    // return db.genKeyForDb(dbName);
  }).then(function (apiCred) {
    return { "Message": "Created a DB for you!", "db": dbName, "api": apiCred };
  }).catch(function (err) {
    if (err.statusCode === 412) {
      // return res.json({"Message": "DB Exists!", "err": err}); }
      // get new credentials.
      // return db.genKeyForDb(dbName).then((apiCred)=>{
      return db.genKeyForMultiDBs(dbNameZ).then(function (apiCred) {
        return { "Message": "DB exists, new creds for you!", "db": dbName, "api": apiCred };
      }).catch(function (err) {
        return { "err": err };
      });
    } else {
      return { "err": err };
    }
  });
}

module.exports = createDbAndGetCreds;