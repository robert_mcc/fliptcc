class AddCouchDbToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :couch_db, :string
  end
end
