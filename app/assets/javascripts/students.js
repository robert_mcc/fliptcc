function sendMessageToParent(method, data){
  if(window.parent && window.parent.postMessage)
    window.parent.postMessage(JSON.stringify({method: method, data: data}), "*");
}

$(function(){
  if($('#student_id')[0]){
    var student_id = $('#student_id').val();
    var data = {
      student_number: $('#student_student_number').val(),
      name: $('#student_name').val(),
      alt_name: $('#student_alt_name').val(),
      id: student_id
    };
    sendMessageToParent('update_student', data);
  }
});

/*
$(function(){
  var showDiv = $('#show_div');
  if(showDiv[0]){
    var data = {
      student_number: $('#student_student_number').text(),
      name: $('#student_name').text(),
      alt_name: $('#student_alt_name').text(),
    };
    sendMessageToParent('update_student', data);
  }
});
*/
