class AddCouchKeyToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :couch_key, :string
  end
end
