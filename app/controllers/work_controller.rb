class WorkController < ApplicationController
  before_action :set_lesson, only: [:show, :view_progress, :own_progress]
  before_action :set_student, only: [:show, :item_done, :own_progress]
  before_action :set_item, only: [:item_done]
  def show
    # chapter = Chapter.new 'get-into-english', 'CH10-1'

    chapter = @lesson.chapter # Chapter.fetch @lesson.chapter_id
    @items = chapter.items
    @title = chapter.title
    @copyright = chapter.copyright
    #@item_idents = chapter.item_idents

    # yaml_items = YAML.load(File.read('db/seeds/english-power-at-work/CH13-1.yaml'))
    # @items = []
    # yaml_items.each do |item|
    #   i = Item.new
    #   i.content = item
    #   @items << i
    # end

  end

  def item_done
    lesson_progress = LessonProgress.new
    lesson_progress.item_ident = @item['item_id']
    lesson_progress.item_finished = @item[:finished]
    lesson_progress.lesson_id = @item[:lesson_id]
    lesson_progress.student_id = @student.id

    if lesson_progress.save
      respond_to do |format|
        format.json {render json: lesson_progress, status: 200}
      end
    else
      respond_to do |format|
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def view_progress
    # these are the items for Unit 10-1
    # @item_idents = [
    #   'U10-1-a',
    #   'U10-1-b',
    #   'U10-1-Q1-question',
    #   'U10-1-Q2-question',
    #   'U10-1-Q3-question',
    #   'U10-1-Q1-MCQ',
    #   'U10-1-Q2-MCQ',
    #   'U10-1-Q3-MCQ',
    #   'U10-1-vocab-1-1',
    #   'U10-1-vocab-1-2',
    #   'U10-1-vocab-1-3',
    #   'U10-1-vocab-1-4',
    #   'U10-1-vocab-1-5',
    #   'U10-1-vocab-2-1',
    #   'U10-1-vocab-2-2',
    #   'U10-1-vocab-2-3',
    #   'U10-1-vocab-2-4',
    #   'U10-1-vocab-2-5',
    # ]
    # yaml_items = YAML.load(File.read('db/seeds/english-power-at-work/CH13-1.yaml'))
    # @item_idents = []
    # @items = []
    # yaml_items.each do |item|
    #   i = Item.new
    #   i.content = item
    #   @items << i
    #   @item_idents.concat(i.idents)
    # end
    # chapter = Chapter.new 'english-power-at-work', 'CH13-1'

    chapter = Chapter.fetch @lesson.chapter_id
    @items = chapter.items
    @item_idents = chapter.item_idents

    @progresses = {}
    LessonProgress.where(lesson_id: @lesson.id).find_each do |progress|
      if(progress.student_id && !@progresses.key?(progress.student_id))
        @progresses[progress.student_id] = {}
      end
      if(progress.student_id)
        @progresses[progress.student_id][progress.item_ident] = progress.item_finished
      end
    end
    student_keys = @progresses.keys
    @students = {}
    Student.find(student_keys).each do |student|
      @students[student.id] = student
    end

    if params[:csv]
      # render :csv, :layout => false
      render :csv
      return 
    end
    if params[:truecsv]
      # render :csv, :layout => false
      render :truecsv, :layout => false
      return 
    end


    respond_to do |format|
      format.html
      format.json { 
        # where we count progresses for each student.
        scores = []
        @progresses.each_pair do |student_id, progress|
          plen = progress.length.to_f
          tot = @item_idents.length.to_f
          ave = plen/tot*100
          #score = ave.to_i
          score = ave.to_i
          s = @students[student_id]
          scores << [s.student_number, s.name, s.alt_name, score]
        end
        response.headers['Access-Control-Allow-Origin'] = '*'
        render json: scores
      }
    end

  end

  def progress
    if !params[:updated_at]
      query = LessonProgress.where :lesson_id => params[:lesson_id]
      render json: query
    else
      query = LessonProgress.where 'lesson_id = ? AND updated_at > ?', params[:lesson_id], params[:updated_at]
      render json: query
    end
  end

  def own_progress
    query = LessonProgress.where :student_id => @student.id, :lesson_id => params[:lesson_id]
    render json: query
  end

  private
  #   # Use callbacks to share common setup or constraints between actions.
    def set_lesson
      @lesson = Lesson.find(params[:lesson_id])
    end
    def set_student
      if cookies[:student_number]
        @student = Student.find_by student_number: cookies[:student_number]
        @incoming_student = nil
      else
        student_params = params.fetch(:student, {}).permit(:student_number, :name, :alt_name)
        @incoming_student = Student.find_by student_number: student_params[:student_number]
        if (@incoming_student.nil?)
          @student = Student.new(student_params)
        else
          @student = @incoming_student
        end
      end
    end

    def set_item
      @item = params.permit(:item_id, :finished, :lesson_id)
    end
  #
  #   # Never trust parameters from the scary internet, only allow the white list through.
  #   def lesson_params
  #     params.fetch(:lesson, {}).permit(:lesson_id)
  #   end
end
