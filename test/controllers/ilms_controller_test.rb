require 'test_helper'

class IlmsControllerTest < ActionDispatch::IntegrationTest
  test "should get login" do
    get ilms_login_url
    assert_response :success
  end

  test "should get logout" do
    get ilms_logout_url
    assert_response :success
  end

end
