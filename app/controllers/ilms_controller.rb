class IlmsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def teacher
    teacher_params = params.permit(:teacher_username)
    teacher_username = teacher_params[:teacher_username]
    teacher_email = teacher_username << '@fy.edu.tw'
    teacher = Teacher.find_by email: teacher_email
    render json: teacher
  end

  def course
    course_params = params.permit(:id)
    course_id = course_params[:id]
    course = Course.find course_id
    teacher = course.teacher
    teacher_username = teacher.email.split('@')[0]
    render json: {:course => course, :teacher_username => teacher_username}
  end

  def courses
    course_ids = params[:ids]
    courses = Course.where id: course_ids
    render json: courses
  end

  def couch_creds
    require 'net/http'
    require 'json'
    teacher_params = params.permit(:teacher_username)
    teacher_username = teacher_params[:teacher_username]
    teacher_email = teacher_username << '@fy.edu.tw'
    teacher = Teacher.find_by email: teacher_email
    if teacher.couch_key.nil?
      # get one
      uri = URI.parse("https://wt-rmanninofoo-gmail-com-0.run.webtask.io/cloudant-db-creator/create?user_email=#{teacher_email}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      res = Net::HTTP.get_response(uri)
      puts res.body if res.is_a?(Net::HTTPSuccess)
      r = JSON.parse(res.body)
      key = r['api']['key']
      pass = r['api']['password']
      db = r['db']
      puts key
      puts pass
      puts db
      # save
      teacher.couch_password = pass
      teacher.couch_key = key
      teacher.couch_db = db
      teacher.save!
      puts teacher.inspect
      teacher = Teacher.find teacher.id
      puts teacher.inspect
    end
    render json: teacher
  end


  def create_course
    course_params = params.permit(:id, :name)
    teacher_email = params[:teacher_username] << '@fy.edu.tw'
    teacher = Teacher.find_by email: teacher_email
    course = Course.new(course_params)
    puts course
    puts course_params
    course.teacher = teacher
    respond_to do |format|
      if course.save
        format.json { render json: course }
      else
        # TODO: if it exists, fetch and return
        format.json { render json: course.errors, status: :unprocessable_entity }
      end
    end
  end

  def close_lesson
    # lesson_params = params.permit(:chapter_id, :course_id)
    lesson_id = params[:lesson_id]
    @lesson  = Lesson.find lesson_id
    @lesson.is_open = false
    respond_to do |format|
      if @lesson.save
        format.json { render json: @lesson }
      else
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def show_lesson
    lesson_id = params[:lesson_id]
    @lesson  = Lesson.find lesson_id
    @lesson.is_hidden = false
    if @lesson.save
        render json: @lesson
    else
        render json: @lesson.errors, status: :unprocessable_entity
    end
  end

  def hide_lesson
    lesson_id = params[:lesson_id]
    @lesson  = Lesson.find lesson_id
    @lesson.is_hidden = true
    if @lesson.save
        render json: @lesson
    else
        render json: @lesson.errors, status: :unprocessable_entity
    end
  end

  def open_lesson
    create_lesson
  end

  def create_lesson
    # TODO change this to open_course
    # It creates if does not exist and sets :open flag if :closed
    
    lesson_params = params.permit(:chapter_id, :course_id)
    # lesson_params = {}
    # lesson_params[:chapter_id] = params[:chapter_id]
    # lesson_params[:course_id] = params[:course_id]
    ### lesson_params[:book_id] = params[:book_id]
    teacher_email = params[:teacher_username] << '@fy.edu.tw'
    teacher = Teacher.find_by(email: teacher_email)

    @lesson = Lesson.find_by(lesson_params)
    if @lesson.nil?
      @lesson = Lesson.new(lesson_params)
    end
    @lesson.teacher = teacher
    @lesson.title = @lesson.chapter.title
    # TODO: have a status flag for lessons
    @lesson.is_open = true
    @lesson.is_hidden = false

    respond_to do |format|
      if @lesson.save
        # format.json { render :show, status: :created, location: @lesson }
        format.json { render json: @lesson }
      else
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end


  # accept book_ident
  # include lessons if available
  def chapters_for_course
    book_ident = params[:book_ident]
    course_id = params[:course_id]

    @chapters = Chapter.all.select {|c| c.book_ident == book_ident}
    @chapters.sort! {|c1, c2| c1.title <=> c2.title}
    chapter_ids = @chapters.map{|c| c.id}
    @lessons = Lesson.where :chapter_id => chapter_ids, :course_id => course_id
    chapters_json = @chapters.as_json
    #merge in lessons
    ch_json = chapters_json.map{|c| 
      chapter_id = c["id"]
      lessons_for_chap = @lessons.select {|l| l.chapter_id == chapter_id}
      c["lessons"] = lessons_for_chap.as_json
      c["lesson"] = lessons_for_chap.first.as_json
      c
    }
    respond_to do |format|
      format.json { render json: ch_json }
    end
  end

  def lessons_for_course
    #TODO: check if teacher, if student, only show open classes
    course_id = params[:course_id]
    lessons = Lesson.where(:course_id => course_id, :is_hidden => false)
    respond_to do |format|
      format.json { render json: lessons }
    end
  end

  def create_teacher
    email = params[:teacher_username] << '@fy.edu.tw'
    creds = params[:creds]
    # check it
    generated_password = Devise.friendly_token.first(8)
    teacher = Teacher.create!(:email => email, :password => generated_password)
    respond_to do |format|
      format.json { render json: {:user => teacher, :password => generated_password}}
    end
  end

  def login
    username = params[:username]
    authenticity_token = session[:_csrf_token]
    teacher = Teacher.where(:email => username).first
    sign_in :teacher, teacher
    respond_to do |format|
      format.json { render json: {:user => teacher, :authenticity_token => authenticity_token}}
    end
  end

  def logout
  end
end
