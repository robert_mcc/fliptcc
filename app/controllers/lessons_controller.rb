class LessonsController < ApplicationController
  before_action :set_lesson, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_teacher!

  # GET /lessons
  # GET /lessons.json
  def index
    lp = params.permit(:chapter_id)
    if lp['chapter_id']
      chapter_id = lp['chapter_id']
      @chapter = Chapter.fetch chapter_id
      @lessons = Lesson.where :teacher_id => current_teacher.id, :chapter_id => chapter_id
    else
      @lessons = Lesson.where :teacher_id => current_teacher.id
    end
  end

  # GET /lessons/1
  # GET /lessons/1.json
  def show
    @student = Student.find params[:id]
  end

  # GET /lessons/new
  def new
    lp = params.permit(:chapter_id)
    if lp['chapter_id']
      @chapter_id = lp['chapter_id']
    end
    @lesson = Lesson.new
    @chapters = Chapter.all_as_hashes
  end

  # GET /lessons/1/edit
  def edit
    @chapters = Chapter.all_as_hashes
  end

  # POST /lessons
  # POST /lessons.json
  def create
    @lesson = Lesson.new(lesson_params)
    @lesson.teacher = current_teacher
    # TODO: add courses to website frontend
    # have a guest course
    if @lesson.course_id.nil?
      @lesson.course_id = 1
    end

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to @lesson, notice: 'Lesson was successfully created.' }
        format.json { render :show, status: :created, location: @lesson }
      else
        format.html { render :new }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lessons/1
  # PATCH/PUT /lessons/1.json
  def update
    respond_to do |format|
      if @lesson.update(lesson_params)
        format.html { redirect_to @lesson, notice: 'Lesson was successfully updated.' }
        format.json { render :show, status: :ok, location: @lesson }
      else
        format.html { render :edit }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lessons/1
  # DELETE /lessons/1.json
  def destroy
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to lessons_url, notice: 'Lesson was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lesson
      @lesson = Lesson.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lesson_params
      params.fetch(:lesson, {}).permit(:code, :chapter_id, :title)
    end
end
