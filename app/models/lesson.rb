class Lesson < ApplicationRecord
  has_many :lesson_progresses, dependent: :destroy
  belongs_to :course
  belongs_to :teacher
  def chapter
    if chapter_id
      return Chapter.fetch chapter_id
    else
      return nil
    end
  end
  def students_count
    Lesson.count_by_sql "SELECT COUNT(DISTINCT student_id) FROM lesson_progresses WHERE lesson_id=#{self.id.to_i}"
  end
end
