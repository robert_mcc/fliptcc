class AddCourseIdToLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.references :teacher
    end

    add_reference :lessons, :course,  index: true, foreign_key: true
  end
end
