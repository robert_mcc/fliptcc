class AddChapterIdToLesson < ActiveRecord::Migration[5.0]
  def change
    add_column :lessons, :chapter_id, :integer
  end
end
