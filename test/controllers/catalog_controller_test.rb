require 'test_helper'

class CatalogControllerTest < ActionDispatch::IntegrationTest
  test "should get books" do
    get catalog_books_url
    assert_response :success
  end

  test "should get chapters" do
    get catalog_chapters_url
    assert_response :success
  end

  test "should get lessons" do
    get catalog_lessons_url
    assert_response :success
  end

end
