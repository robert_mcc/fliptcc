class AddStudentRefToLessonProgresses < ActiveRecord::Migration[5.0]
  def change
    add_reference :lesson_progresses, :student, foreign_key: true
  end
end
