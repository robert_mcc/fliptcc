Rails.application.routes.draw do
  get  'ilms/login'
  post 'ilms/login'

  get  'ilms/lessons/:course_id', to: 'ilms#lessons_for_course'
  post 'ilms/create_lesson', to: 'ilms#create_lesson'
  post 'ilms/open_lesson',   to: 'ilms#open_lesson'
  post 'ilms/close_lesson',  to: 'ilms#close_lesson'
  post 'ilms/show_lesson',   to: 'ilms#show_lesson'
  post 'ilms/hide_lesson',  to: 'ilms#hide_lesson'
  post 'ilms/create_course', to: 'ilms#create_course'
  post 'ilms/create_teacher', to: 'ilms#create_teacher'
  get  'ilms/teacher',       to: 'ilms#teacher'
  get  'ilms/course',        to: 'ilms#course'
  get  'ilms/courses',        to: 'ilms#courses'
  get  'ilms/chapters_for_course', to: 'ilms#chapters_for_course'
  get  'ilms/couch_creds',        to: 'ilms#couch_creds'

  get  'ilms/logout'

  devise_for :teachers
  get 'catalog/books'

  get 'catalog/books/:book_ident/chapters', to: 'catalog#chapters'

  get 'catalog/books/:book_ident/chapters/:chapter_id/lessons', to: 'catalog#lessons'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
  root 'welcome#index'

  resources :lessons
  resources :students
  get 'students/set_name/:student_number', to: 'students#set_name'
  get 'students_collection', to: 'students#students_collection'
  get 'students_collection_from_alt_names', to: 'students#students_collection_from_alt_names'
  get 'students_in_course/:course_id', to: 'students#students_in_course'

  get '/work/:lesson_id', to: 'work#show'
  get '/work/:lesson_id/item-done/:item_id', to: "work#item_done"
  get '/work/:lesson_id/view-progress', to: 'work#view_progress'
  post '/signin-student', to: 'signin_student#signInOrCreate'

  post '/work/:lesson_id', to: 'work#show'

  get '/work/:lesson_id/own_progress', to: 'work#own_progress'
end
