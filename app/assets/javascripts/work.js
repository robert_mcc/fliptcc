var itemDone = function(itemId){
  if(!window.fliptcc.doneItems[itemId]){
    window.fliptcc.doneItems[itemId] = true;
    markAsDone(itemId);
    $.ajax({
      url: '/work/' + window.fliptcc.lessonId + '/item-done/' + itemId,
      type: 'GET',
      dataType: 'json',
      data: {
        lesson_id: window.fliptcc.lessonId,
        item_id: itemId,
        finished: true
      },
      success: function() {
        console.log('finished ' + itemId);
        displayPercent();
      },
      error: function() {
        console.log('error logging ' + itemId);
        alert("Oh no! Could not connect to the internet!");
      }
    });
  }
};

var displayPercent = function(){
  if(Object.keys){
    var percentDone = Math.round(Object.keys(window.fliptcc.doneItems).length / window.fliptcc.itemIdents.length * 100);
    $('.progress-bar').attr('style', "width: " + percentDone + "%;").attr('aria-valuenow', percentDone + '%').html(percentDone + '%');
  }
};

var markAsDone = function(itemId) {
  $("#" + itemId + " .checked-span").show();
  $("#" + itemId + " .unchecked-span").hide();
};

var fetchDone = function(){
  return $.when($.ajax({
    url: '/work/' + window.fliptcc.lessonId + '/own_progress',
    type: 'GET',
    dataType: 'json'
  })).then(function(items){
    for(var i = 0; i<items.length; i++) {
      markAsDone(items[i].item_ident);
      window.fliptcc.doneItems[items[i].item_ident] = true;
    }
    displayPercent();
  });
};

var playAudio = function(itemId){
    var selector = "#audio-" + itemId;
    $(selector).play();
};

var setupAudioItem = function(itemId, length){
    $("#audio-" + itemId).on('playing', function(){
        window.setTimeout(function(){
            itemDone(itemId);
        }, length);
    });
    $("#text-" + itemId).on("click", function(){
        var audio = document.getElementById("audio-" + itemId);
        if (audio.paused){
            audio.play();
        }
        else{
           audio.pause();
        }
    });
};

var checkTextItem = function(textInput, answer) {
    var val, valApost1, valApost2;
    val = textInput.val();
    if(val){
      val = val.trim();
      valApost1 = val.replace(/’/g, "'");
      valApost2 = val.replace(/'/g, "’");
    }
    else{
      valApost1 = val;
      valApost2 = val;
    }
    if(answer){
      answer = answer.trim();
    }
    // deal with apostrophe problem
    if(val && (val === answer || valApost1 === answer || valApost2 === answer)){
        return true;
    }
    else {
        return false;
    }
};
var progressiveCheckTextItem = function(textInput, answer) {
    var val, valApost1, valApost2;
    val = textInput.val();
    if(val) {
      val = val.trim();
      valApost1 = val.replace(/’/g, "'");
      valApost2 = val.replace(/'/g, "’");
    }
    else{
      valApost1 = val;
      valApost2 = val;
    }
    if(answer) answer = answer.trim();
    if(val && (0 === answer.indexOf(val)       || 
               0 === answer.indexOf(valApost1) || 
               0 === answer.indexOf(valApost2)    )
    ){
        return true;
    }
    else {
        return false;
    }
};

var setupDictalItem = function(itemId, answer){
    var textInput = $("#" + itemId + " input");
    var onChange = function(){
        if(progressiveCheckTextItem(textInput, answer)) {
            textInput.removeClass("wrong");
            if(checkTextItem(textInput, answer)){
                itemDone(itemId);
            }
        }
        else {
            textInput.addClass("wrong");
        }
    }
    $("#text-" + itemId).on("click", function(){
        var audio = document.getElementById("audio-" + itemId);
        if (audio.paused){
            audio.play();
        }
        else{
           audio.pause();
        }
    });
    textInput.on("input", onChange);
    // textInput.on("keyup", onChange);
    // textInput.change(onChange);
};

var checkMCQItem = function(radioName, answer){
    var val = $("input[name=" + radioName +  "]:checked").val();
    if(val === answer) {
        return true;
    }
    else {
        return false;
    }
};

var disableRadioForSeconds = function(radioName, milliseconds) {
    $("input[name=" + radioName + "]").attr("disabled",true);
    if(milliseconds){
        window.setTimeout(function(){
            console.log('enable radio');
                $("input[name=" + radioName + "]").attr("disabled",false);
        }, milliseconds);
    }
}

var setUpListeningMCQ = function(itemId, answer, milliseconds) {
    var radioName = itemId;
    disableRadioForSeconds(radioName, 0);
    $("#audio-" + itemId).on("playing", function(){
        disableRadioForSeconds(radioName, milliseconds);
    });

    $("input[name=" + radioName +  "]").change(function(){
        console.log("radio changed");
        var right = checkMCQItem(radioName, answer);
        if(right) {
            $("input[name=" + radioName +  "]:checked").addClass("right");
            itemDone(itemId);
        } else {
            $("input[name=" + radioName +  "]:checked").addClass("wrong");
            disableRadioForSeconds(radioName, 10000);
            $("#audio-" + itemId)[0].play();
        }
    });
};

var setUpClozeItem = function(itemId, answer) {
    // same as dictal item.
    var textInput = $("#" + itemId + " input");
    var onChange = function(){
        if(progressiveCheckTextItem(textInput, answer)) {
            textInput.removeClass("wrong");
            if(checkTextItem(textInput, answer)){
                itemDone(itemId);
            }
        }
        else {
            textInput.addClass("wrong");
        }
    }
    textInput.on("input", onChange);
    // textInput.on("keyup", onChange);
    // textInput.change(onChange);
};

// var createTds = function(item_idents){
//   var buff = [];
//   for(int i = 0; i< item_idents; i++) {
//     buff.append("<td class='" + item_idents[i] + "'>" + "</td>");
//   }
//   return buff.join("");
// };
//
// var createRow = function(student, item_idents) {
//   var row = $("<tr>" + createTds(item_idents) + "</tr>");
//   row.addClass("student-" + student.id);
//   return row;
// };

window.fliptcc = {};
window.fliptcc.doneItems = {};
// $("#audio-U10-1-a").on('playing', function(){
//     console.log('playing')
//     window.setTimeout(function(){
//         console.log('played');
//         itemDone("#U10-1-a");
// }, 37000);
