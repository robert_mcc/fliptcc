class AddItemNumberToLessonProgress < ActiveRecord::Migration[5.0]
  def change
    add_column :lesson_progresses, :item_number, :integer
    add_column :lesson_progresses, :item_finished, :boolean
  end
end
