class AddCouchPasswordToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :couch_password, :string
  end
end
