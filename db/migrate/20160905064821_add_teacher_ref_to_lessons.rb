class AddTeacherRefToLessons < ActiveRecord::Migration[5.0]
  def change
    add_reference :lessons, :teacher, index: true, foreign_key: true
  end
end
