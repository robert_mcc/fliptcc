class CatalogController < ApplicationController
  before_action :set_books
  before_action :set_book, :only => [:chapters, :lessons]
  def books
      respond_to do |format|
        format.json { render :json => @books }
        format.html {render}
      end
  end

  def chapters
      @chapters = Chapter.all.select{|c| c.book_ident == @book_ident}
      # @chapters.sort! {|x,y| x.title <=> y.title}
      # All this does is sort by title but, make 2 come before 10
      @chapters.sort_by! do |x|
         title = x.title
         numbers = title.split(" ")
         arr = numbers.map do |word|
           if word.to_i > 0
             "%03d" % word.to_i
           else
             word
           end
         end
         arr.join(" ")
      end

      respond_to do |format|
        format.html {render}
        format.json { render :json => @chapters }
      end
  end

  def lessons
    lesson_params = params.permit(:book_ident, :chapter_id)
    @chapter = Chapter.fetch lesson_params[:chapter_id].to_i
    if current_teacher
      @lessons = Lesson.where(:chapter_id => lesson_params[:chapter_id], :teacher => current_teacher)
    else
      @lessons = Lesson.where(:chapter_id => lesson_params[:chapter_id], :teacher => nil)  # for demo
    end
    @completed = []
  end

  private
  def set_books
    # list books
    # We just have two books, so we won't create a model just yet.
    @books = [{'title' => 'Get into English',
               'ident' => 'get-into-english',
               'img_src' =>'/images/get-into-english-cover.png',
               'blurb' => 'Get into English, 曹嘉秀.方淑慧等, All-People Publishing, 2015, 978-986-5765-12-5.'
              },
              {'title'  => 'English Power at Work',
               'ident' => 'english-power-at-work',
               'img_src' => '/images/english-power-at-work-cover.jpg',
               'blurb' => 'English Power @ Work, 曹家秀, AMC Publishing Company, 2015, 978-986-5965-72-3.'
              },
              {'title'  => 'Lets Talk 2',
               'ident' => 'lets-talk-2',
               'img_src' => '/images/lets-talk-2-cover.png',
               'blurb' => 'Jones, Leo, Lets Talk 2, Cambridge UP',
               'private' => true
              },
              {'title'  => 'Rodney vs. Death',
               'ident' => 'rodney-vs-death',
               'img_src' => '/images/lets-talk-2-cover.png',
               'blurb' => 'TAL',
               'private' => false
              }
    ]
  end
  def set_book
      book_params = params.permit(:book_ident)
      @book_ident = book_params[:book_ident]
      book_index = @books.find_index {|b| b['ident'] == @book_ident}
      @book = @books[book_index]
  end
end
