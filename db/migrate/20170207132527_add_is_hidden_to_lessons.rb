class AddIsHiddenToLessons < ActiveRecord::Migration[5.0]
  def change
    add_column :lessons, :is_hidden, :boolean
  end
end
