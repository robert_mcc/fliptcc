class StudentsController < ApplicationController
  # for the iFrame -- safari doesn't allow 3rd party cookies
  skip_before_action :verify_authenticity_token

  # GET /students/1
  # GET /students/1.json
  def show
    allow_XFrame
    @student = Student.find params[:id]
    render :layout => false, :layout => 'set_name'
  end

  def students_collection
    student_numbers = params[:student_numbers]
    @students = Student.where("student_number IN (?)", student_numbers)
    render json: @students
  end

  def students_collection_from_alt_names
    chinese_names = params[:alt_names]
    @students = Student.where("alt_name IN (?)", chinese_names)
    render json: @students
  end

  def students_in_course
    course_id = params[:course_id]
    @students = Student.where(:course_id => course_id)
    render json: @students
  end

  def set_name
    allow_XFrame
    @student = Student.find_by :student_number => params[:student_number]
    course_id = params[:course_id]
    @read_only_form = params[:read_only]
    if !@student.nil?
      # @student.attributes = student_params
      @student.course_id = course_id
      render :edit, :layout => 'set_name'
    else
      @student = Student.new :student_number => params[:student_number]
      @student.course_id = course_id
      render :edit, :layout => 'set_name'
    end
  end

  def create
    allow_XFrame
    @student = Student.find_or_initialize_by :student_number => student_params[:student_number],
                                             :course_id      => student_params[:course_id]
    @student.assign_attributes student_params
    #@student = Student.new student_params
    if @student.save
      render :show, :layout => 'set_name'
    end
  end
  def update
    allow_XFrame
    @student = Student.find params[:id]
    if @student.update student_params
      render :show, :layout => 'set_name'
    end
  end

  def edit
    allow_XFrame
    @student = nil
    if student_params[:student_number]
      @student = Student.find_by 'student_number = ?', student_params[:student_number]
    elsif params[:id]
      @student = Student.find params[:id]
    end
    if @student != nil
      @student.attributes = student_params
    else
      @student = Student.new(student_params)
      if !@student.student_number.nil?
        @student.student_number.downcase!
      end
    end
    render :edit, :layout => false
  end

    def student_params
      params.fetch(:student, {}).permit(:student_number, :name, :alt_name, :course_id)
    end
    def lesson_id
      params.fetch(:lesson_id)
    end

    def allow_XFrame
      response.headers.delete "X-Frame-Options"
      response.headers["X-FRAME-OPTIONS"] = "ALLOWALL"
    end
end
