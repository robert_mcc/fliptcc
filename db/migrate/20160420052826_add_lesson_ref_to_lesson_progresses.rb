class AddLessonRefToLessonProgresses < ActiveRecord::Migration[5.0]
  def change
    add_reference :lesson_progresses, :lesson, foreign_key: true
  end
end
