class SigninStudentController < ApplicationController
  def signInOrCreate
    @student = Student.find_by 'student_number = ?', student_params[:student_number]
    if @student != nil
      @student.attributes = student_params
    else
      @student = Student.new(student_params)
      @student.student_number.downcase!
    end
    lesson_url = '/work/' + lesson_id
    respond_to do |format|
      if @student.save
        cookies[:student_number] = @student.student_number
        puts "set cookie #{cookies[:student_number]}" + cookies[:student_number]
        format.html { redirect_to  lesson_url}
        #format.json { render :show, status: :created, location: @lesson }
      #else
        #format.html { render :new }
        #format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

    def student_params
      params.fetch(:student, {}).permit(:student_number, :name, :alt_name)
    end
    def lesson_id
      params.fetch(:lesson_id)
    end
end
