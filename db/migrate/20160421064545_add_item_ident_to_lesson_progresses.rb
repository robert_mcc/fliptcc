class AddItemIdentToLessonProgresses < ActiveRecord::Migration[5.0]
  def change
    add_column :lesson_progresses, :item_ident, :string
    remove_column :lesson_progresses, :item_number, :integer
  end
end
