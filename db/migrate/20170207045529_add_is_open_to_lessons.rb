class AddIsOpenToLessons < ActiveRecord::Migration[5.0]
  def change
    add_column :lessons, :is_open, :boolean
  end
end
