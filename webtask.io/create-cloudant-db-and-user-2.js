"use strict";

const Cloudant = require('cloudant');

var Express = require('express');
var Webtask = require('webtask-tools');
var bodyParser = require('body-parser');


var server = Express();
server.use(bodyParser.json());

var corsHeaders = function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
};

class Database {

  connect(me, pass){
    this.cld = Cloudant({account: me, password: pass});
  }
  
  createDb(dbName){
    this.name = dbName;
    return new Promise((resolve, reject)=>{
      this.cld.db.create(dbName, (err, body) => {
        if(err) return reject(err);
        else return resolve(body);
      })
    });
  }

  genKey() {
    const promise = new Promise((resolve, reject) => {
      this.cld.generate_api_key((err, api) => {
        if (err) return err; 
        else return resolve(api);
      });
    });
    return promise;
  }
  
  authDbForKey(apiCred, dbName) {
    let security = {};
    dbName = dbName || this.dbName;
    // set security
    var my_database = this.cld.db.use(dbName);
    return new Promise((resolve, reject) =>{

      const getSecurityPromise = new Promise((resolve, reject)=>{
        my_database.get_security((err, result)=>{
          if (err) return reject(err);
          else     return resolve(result);
        });
      });
      getSecurityPromise.then((security)=>{
        security[apiCred.key] = [ '_reader', '_writer', '_replicator'];
        my_database.set_security(security, function(err, result) {
          if (err) return reject(err);
          else     return resolve(result);
        });
      });
    });
  } 
  
  genKeyForDb(dbName) {
    const self = this;
    let apiCred;
    return this.genKey().then((api) => {
      apiCred = api;
      return this.authDbForKey(apiCred, dbName);
    }).then(()=>apiCred);
  }

  genKeyForMultiDBs(DBs) {
    const self = this;
    let apiCred;
    return this.genKey().then((api) => {
      apiCred = api;
      let promises = DBs.map( (dbName) => this.authDbForKey(apiCred, dbName) );
      return Promise.all(promises).then( ()=>apiCred );
    });
  }

}


// TODO: require a secret to execute from rails
server.get('/create', function (req, res) {
  const context = req.webtaskContext;
  const secrets = context.secrets;

  const me = secrets.CLOUDANT_USER;
  const mypassword = secrets.CLOUDANT_SECRET;
  // const auth0Id = req.user.sub;
  // const dbName = auth0Id.replace("|", "_")
  const userEmail = req.query.user_email;
  const dbName = userEmail.replace("@", "()").replace(/\./g, '$').toLowerCase();
  console.log('creating: ' + dbName);
  
  var db = new Database();
  db.connect(me, mypassword);
  const dbNameZ = [dbName, dbName + '-images'];

  db.createDb(dbName)
    .then(()=>db.createDb(dbNameZ[1]))
    .then(()=>{
     return db.genKeyForMultiDBs(dbNameZ);
    // return db.genKeyForDb(dbName);
  }).then((apiCred)=>{
    return res.json({"Message": "Created a DB for you!", "db": dbName, "api": apiCred});
  }).catch((err)=>{
    if(err.statusCode === 412){
      // return res.json({"Message": "DB Exists!", "err": err}); }
      // get new credentials.
      // return db.genKeyForDb(dbName).then((apiCred)=>{
      return db.genKeyForMultiDBs(dbNameZ).then((apiCred)=>{
        return res.json({"Message": "DB exists, new creds for you!", "db": dbName, "api": apiCred});
      }).catch( (err) => res.json({"err": err}) );
    }
    else{ return res.json({"err": err}); }
  });
});


// module.exports = Webtask.fromExpress(server).auth0();
module.exports = Webtask.fromExpress(server);
